package com.image.processor.images.store;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ImageStoreConfig {

    private static final String LOCATION_PROPERTIES = "${image.processing.store-location}";

    @Bean
    ImageStore imageStore(@Value(LOCATION_PROPERTIES) String location) {
        return new FileImageStore(location);
    }
}
