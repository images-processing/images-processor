package com.image.processor.images.processing.strategies;

import com.image.processor.model.ImageProcessModel;
import com.image.processor.images.processing.services.ImageProcessorService;

public interface ImageProcessingStrategy {

    ImageProcessorService getProcessingService(final ImageProcessModel processModel);
}
