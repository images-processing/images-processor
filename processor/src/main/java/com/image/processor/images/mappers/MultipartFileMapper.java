package com.image.processor.images.mappers;

import lombok.NoArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@NoArgsConstructor
public final class MultipartFileMapper {

    private static final String IMAGE_CONTENT_TYPE = "image/";

    public static MockMultipartFile map(ZipFile zipFile, ZipEntry zip) throws IOException {
        final String contentType = IMAGE_CONTENT_TYPE + FilenameUtils.getExtension(zip.getName());
        return new MockMultipartFile(FilenameUtils.getBaseName(zip.getName()), zip.getName(), contentType, zipFile.getInputStream(zip));
    }

    public static MockMultipartFile map(MultipartFile file, byte[] bytes) {
        return new MockMultipartFile(file.getName(), file.getOriginalFilename(), file.getContentType(), bytes);
    }
}
