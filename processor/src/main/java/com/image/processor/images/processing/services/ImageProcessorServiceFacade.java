package com.image.processor.images.processing.services;

import com.image.processor.enums.EffectEnum;
import com.image.processor.images.processing.executors.ImageFilterExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.awt.image.BufferedImageOp;
import java.util.List;
import java.util.Map;

@Configuration
class ImageProcessorServiceFacade {

    private static final String SINGLE_IMAGE_PROCESSING_SUPPORTED_FILE_EXTENSIONS =
            "${image.processing.single.supported-file-extensions}";
    private static final String MULTIPLE_IMAGE_PROCESSING_SUPPORTED_FILE_EXTENSIONS =
            "${image.processing.multiple.supported-file-extensions}";

    @Bean
    public ImageProcessorService singleImageProcessorService(
            @Value(SINGLE_IMAGE_PROCESSING_SUPPORTED_FILE_EXTENSIONS) List<String> supportedExtensionsInSingleProcessing,
            Map<EffectEnum, BufferedImageOp> mapOfFilters) {
        return new SingleImageProcessorService(supportedExtensionsInSingleProcessing, new ImageFilterExecutor(mapOfFilters));
    }

    @Bean
    public ImageProcessorService multipleImageProcessorService(
            @Value(MULTIPLE_IMAGE_PROCESSING_SUPPORTED_FILE_EXTENSIONS) List<String> supportedExtensionsInMultipleProcessing,
            @Value(SINGLE_IMAGE_PROCESSING_SUPPORTED_FILE_EXTENSIONS) List<String> supportedExtensionsInSingleProcessing,
            ImageProcessorService singleImageProcessorService) {
        return new MultipleImageProcessorService(
                supportedExtensionsInSingleProcessing,
                supportedExtensionsInMultipleProcessing,
                singleImageProcessorService
        );
    }
}
