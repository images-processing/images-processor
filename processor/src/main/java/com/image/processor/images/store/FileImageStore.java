package com.image.processor.images.store;

import com.image.processor.enums.EffectEnum;
import com.image.processor.enums.ProcessingErrorEnum;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.model.ImageProcessModel;
import com.image.processor.model.image.CustomBufferedImage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.image.processor.utils.ImageFileUtils.*;

@Slf4j
@RequiredArgsConstructor
class FileImageStore implements ImageStore {

    private final String storingLocation;

    @Override
    public List<BufferedImage> store(List<BufferedImage> images, ImageProcessModel model) {
        log.debug("Saving {} images to {}  ...", images.size(), storingLocation + DIRECTORY_DELIMITER + model.getUsername());
        images.stream()
                .map(image -> (CustomBufferedImage) image)
                .forEach(image -> saveFile(image, model));
        return images;
    }

    private void saveFile(CustomBufferedImage image, ImageProcessModel model) {
        try {
            ImageIO.write(image, image.getImageFormat(), buildFile(image, model));
        } catch (IOException e) {
            log.error("Saving file error {}", image.getImageName());
            throw new ImageProcessingException(ProcessingErrorEnum.INCORRECT_PROCESS_FILE);
        }
    }

    private File buildFile(CustomBufferedImage image, ImageProcessModel model) throws IOException {
        final String directoryPath = storingLocation + DIRECTORY_DELIMITER + model.getUsername();
        Files.createDirectories(Path.of(directoryPath));
        return new File(buildPath(image, directoryPath, model.getEffect()));
    }

    private String buildPath(CustomBufferedImage image, String directoryPath, EffectEnum effect) {
        return directoryPath + DIRECTORY_DELIMITER + image.getImageName().replace(FILE_NAME_DELIMITER, SINGLE_SPACE) +
                FILE_NAME_DELIMITER + effect.effectName() + FILE_NAME_DELIMITER +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)) + EXTENSION_DELIMITER + image.getImageFormat();
    }
}
