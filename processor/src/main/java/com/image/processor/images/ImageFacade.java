package com.image.processor.images;

import com.image.processor.model.ImageInfoModel;
import com.image.processor.model.ImageInfoModelWithUsername;
import com.image.processor.model.ImageProcessModel;

import java.util.Set;

public interface ImageFacade {

    void processImage(ImageProcessModel model);

    Set<ImageInfoModel> fetchAllImages(String username);

    String fetchImage(ImageInfoModelWithUsername imageInfoModel);
}
