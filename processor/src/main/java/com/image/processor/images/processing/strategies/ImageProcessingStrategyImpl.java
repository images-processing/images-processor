package com.image.processor.images.processing.strategies;

import com.image.processor.enums.ProcessingErrorEnum;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.model.ImageProcessModel;
import com.image.processor.images.processing.services.ImageProcessorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
class ImageProcessingStrategyImpl implements ImageProcessingStrategy {

    private final List<ImageProcessorService> imageProcessorServices;

    @Override
    public ImageProcessorService getProcessingService(final ImageProcessModel processModel) {
        log.debug("Adjusting suitable processing service ...");
        final var fileExtension = getFileExtension(processModel.getFile());
        return imageProcessorServices.stream()
                .filter(service -> service.support(fileExtension))
                .findFirst()
                .orElseThrow(() -> new ImageProcessingException(ProcessingErrorEnum.UNSUPPORTED_FILE_EXTENSION));
    }

    private String getFileExtension(MultipartFile file) {
        return Optional.ofNullable(file)
                .map(MultipartFile::getOriginalFilename)
                .map(FilenameUtils::getExtension)
                .orElseThrow(() -> new ImageProcessingException(ProcessingErrorEnum.UNSUPPORTED_FILE_EXTENSION));
    }
}
