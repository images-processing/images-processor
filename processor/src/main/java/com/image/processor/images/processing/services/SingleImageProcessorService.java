package com.image.processor.images.processing.services;

import com.image.processor.enums.ProcessingErrorEnum;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.images.processing.executors.ImageFilterExecutor;
import com.image.processor.model.ImageProcessModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
class SingleImageProcessorService implements ImageProcessorService {

    private final List<String> supportedFileExtensions;
    private final ImageFilterExecutor filterExecutor;

    @Override
    public boolean support(String fileExtension) {
        return supportedFileExtensions.contains(fileExtension);
    }

    @Override
    public List<BufferedImage> process(ImageProcessModel processModel) {
        log.debug("Processing single file {}", processModel.getFile().getOriginalFilename());
        return Optional.ofNullable(processModel.getFile())
                .filter(file -> !file.isEmpty())
                .map(file -> filterExecutor.execute(processModel))
                .map(Arrays::asList)
                .orElseThrow(() -> new ImageProcessingException(ProcessingErrorEnum.INCORRECT_PROCESS_FILE));
    }

}
