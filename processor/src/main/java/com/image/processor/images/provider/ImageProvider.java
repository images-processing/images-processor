package com.image.processor.images.provider;

import com.image.processor.model.ImageInfoModel;
import com.image.processor.model.ImageInfoModelWithUsername;

import java.util.Set;

public interface ImageProvider {

    Set<ImageInfoModel> fetchAllByUsername(String username);

    String fetchImage(ImageInfoModelWithUsername imageInfoModel);
}
