package com.image.processor.images.provider;

import com.image.processor.exceptions.FunctionWithException;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.model.ImageInfoModel;
import com.image.processor.model.ImageInfoModelWithUsername;
import com.image.processor.utils.ImageFileUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static com.image.processor.utils.ImageFileUtils.DIRECTORY_DELIMITER;

@Slf4j
@RequiredArgsConstructor
class FileImageProvider implements ImageProvider {

    private static final String IMAGE_NOT_FOUND_MESSAGE = "Image with name %s not found!";
    private final String location;

    @Override
    public Set<ImageInfoModel> fetchAllByUsername(String username) {
        final File userImageDirectory = new File(location + DIRECTORY_DELIMITER + username);
        log.debug("Fetching images from directory : {}", userImageDirectory.getAbsolutePath());
        return fetchAllFilerFromFolder(userImageDirectory)
                .stream().map(fileName -> fileName.split(ImageFileUtils.FILE_NAME_DELIMITER))
                .map(fileNameCompositions -> ImageInfoModelMapper.map(fileNameCompositions, username, location))
                .collect(Collectors.toSet());
    }

    @Override
    public String fetchImage(ImageInfoModelWithUsername imageInfoModel) {
        return Optional.of(new File(ImageInfoModelMapper.map(imageInfoModel, location)))
                .filter(File::exists)
                .map(FunctionWithException.wrapper(file ->
                        Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath())),
                        new ResponseStatusException(
                                HttpStatus.NOT_FOUND, String.format("Not found correct image file with name %s", imageInfoModel.getName())
                        )
                ))
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, String.format(IMAGE_NOT_FOUND_MESSAGE, imageInfoModel.getName())
                ));
    }

    private List<String> fetchAllFilerFromFolder(final File folder) {
        List<String> fileList = new ArrayList<>();
        if (!folder.exists() || !folder.isDirectory() || folder.listFiles() == null) return fileList;
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                fetchAllFilerFromFolder(fileEntry);
            } else {
                fileList.add(fileEntry.getParent() + File.separator + fileEntry.getName() );
            }
        }
        return fileList;
    }
}
