package com.image.processor.images;

import com.image.processor.images.processing.strategies.ImageProcessingStrategy;
import com.image.processor.images.provider.ImageProvider;
import com.image.processor.images.store.ImageStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

@Configuration
class ImageFacadeConfig {

    @Bean
    public ImageFacade imageProcessorFacade(ImageProcessingStrategy processingStrategy,
                                            ImageStore fileImageStore,
                                            ImageProvider imageProvider,
                                            TaskExecutor processingExecutor) {
        return new ImageFacadeImpl(processingStrategy, fileImageStore, imageProvider, processingExecutor);
    }
}
