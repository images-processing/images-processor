package com.image.processor.images.store;

import com.image.processor.model.ImageProcessModel;

import java.awt.image.BufferedImage;
import java.util.List;

public interface ImageStore {

    List<BufferedImage> store(List<BufferedImage> images, ImageProcessModel username);
}
