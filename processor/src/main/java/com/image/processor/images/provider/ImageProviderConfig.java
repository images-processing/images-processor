package com.image.processor.images.provider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ImageProviderConfig {

    private static final String LOCATION_PROPERTIES = "${image.processing.store-location}";

    @Bean
    ImageProvider imageProvider(@Value(LOCATION_PROPERTIES) String location) {
        return new FileImageProvider(location);
    }
}
