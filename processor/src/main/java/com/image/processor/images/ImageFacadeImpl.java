package com.image.processor.images;

import com.image.processor.enums.ProcessingErrorEnum;
import com.image.processor.exceptions.FunctionWithException;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.images.mappers.MultipartFileMapper;
import com.image.processor.images.processing.services.ImageProcessorService;
import com.image.processor.images.processing.strategies.ImageProcessingStrategy;
import com.image.processor.images.provider.ImageProvider;
import com.image.processor.images.store.ImageStore;
import com.image.processor.model.ImageInfoModel;
import com.image.processor.model.ImageInfoModelWithUsername;
import com.image.processor.model.ImageProcessModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RequiredArgsConstructor
class ImageFacadeImpl implements ImageFacade {

    private final ImageProcessingStrategy processingStrategy;
    private final ImageStore imageStore;
    private final ImageProvider imageProvider;
    private final TaskExecutor processingExecutor;

    @Override
    public void processImage(final ImageProcessModel model) {
        log.debug("Processing image...");
        ImageProcessorService processingService = processingStrategy.getProcessingService(model);
        model.setFile(copyFileBytesFromServlet(model));
        processingExecutor.execute(() -> {
            List<BufferedImage> processedImages = processingService.process(model);
            imageStore.store(processedImages, model);
        });
    }

    private MockMultipartFile copyFileBytesFromServlet(ImageProcessModel model) {
        return Optional.ofNullable(model.getFile())
                    .map(FunctionWithException.wrapper(
                            MultipartFile::getBytes,
                            new ImageProcessingException(ProcessingErrorEnum.EMPTY_FILE)))
                    .map(bytes -> MultipartFileMapper.map(model.getFile(), bytes))
                    .orElseThrow(() -> new ImageProcessingException(ProcessingErrorEnum.INCORRECT_PROCESS_FILE));
    }

    @Override
    public Set<ImageInfoModel> fetchAllImages(String username) {
        log.debug("Fetching images for user with name : {}", username);
        return imageProvider.fetchAllByUsername(username);
    }

    @Override
    public String fetchImage(ImageInfoModelWithUsername imageInfoModel) {
        log.debug("Fetching image {} for user with name : {}", imageInfoModel, imageInfoModel.getUsername());
        return imageProvider.fetchImage(imageInfoModel);
    }
}
