package com.image.processor.images.processing.services;

import com.image.processor.model.ImageProcessModel;

import java.awt.image.BufferedImage;
import java.util.List;

public interface ImageProcessorService {

    boolean support(String fileExtension);
    List<BufferedImage> process(ImageProcessModel processModel);
}
