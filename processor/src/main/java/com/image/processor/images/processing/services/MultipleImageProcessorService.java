package com.image.processor.images.processing.services;

import com.image.processor.enums.ProcessingErrorEnum;
import com.image.processor.exceptions.FunctionWithException;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.images.mappers.MultipartFileMapper;
import com.image.processor.model.ImageProcessModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@RequiredArgsConstructor
@Slf4j
class MultipleImageProcessorService implements ImageProcessorService {

    private static final String BLOCKED_SIGN = "/";
    private static final Charset UNIVERSAL_CHARSET = Charset.forName("CP866");

    private final List<String> supportedExtensionsInSingleProcessing;
    private final List<String> supportedFileExtensions;
    private final ImageProcessorService singleImageProcessorService;

    @Override
    public boolean support(String fileExtension) {
        return supportedFileExtensions.contains(fileExtension);
    }

    @Override
    public List<BufferedImage> process(ImageProcessModel processModel) {
        log.debug("Processing zip file {}", processModel.getFile().getOriginalFilename());
        return getFilesFromZip(processModel.getFile()).stream()
                .map(file -> processSingleFile(processModel, file))
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<BufferedImage> processSingleFile(ImageProcessModel processModel, MultipartFile file) {
        processModel.setFile(file);
        return singleImageProcessorService.process(processModel);
    }

    private List<MultipartFile> getFilesFromZip(MultipartFile file) {
        try {
            log.debug("Unzipping file {} ... ", file.getOriginalFilename());
            File tempFile = File.createTempFile("temp", "file");
            FileUtils.writeByteArrayToFile(tempFile, file.getBytes());
            return getFiles(new ZipFile(tempFile, UNIVERSAL_CHARSET));
        } catch (IOException ex) {
            log.error("Unzipping file error {}", file.getOriginalFilename(), ex);
            throw new ImageProcessingException(ProcessingErrorEnum.INCORRECT_PROCESS_FILE);
        }
    }

    private List<MultipartFile> getFiles(ZipFile zipFile) {
        return zipFile.stream()
                .filter(this::isSupportedImage)
                .map(FunctionWithException.wrapper(
                        zip -> MultipartFileMapper.map(zipFile, zip),
                        new ImageProcessingException(ProcessingErrorEnum.INCORRECT_PROCESS_FILE)
                )).collect(Collectors.toList());
    }

    private boolean isSupportedImage(ZipEntry zip) {
        String fileName = zip.getName();
        return supportedExtensionsInSingleProcessing.contains(FilenameUtils.getExtension(fileName))
                && !fileName.contains(BLOCKED_SIGN);
    }

}
