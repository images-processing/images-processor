package com.image.processor.images.processing.executors;

import com.image.processor.enums.EffectEnum;
import com.image.processor.enums.ProcessingErrorEnum;
import com.image.processor.exceptions.ImageProcessingException;
import com.image.processor.model.ImageProcessModel;
import com.image.processor.model.image.CustomBufferedImage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class ImageFilterExecutor {

    private final Map<EffectEnum, BufferedImageOp> mapOfFilters;

    public BufferedImage execute(ImageProcessModel model) {
        String originalFilename = model.getFile().getOriginalFilename();
        try {
            log.debug("Executing filter for effect : {}", model.getEffect());
            var image = ImageIO.read(model.getFile().getInputStream());
            var processedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
            return Optional.ofNullable(mapOfFilters.get(model.getEffect()))
                    .map(filter -> filter.filter(image, processedImage))
                    .map(customImage -> prepareImage(originalFilename, customImage))
                    .orElseThrow(() -> new ImageProcessingException(ProcessingErrorEnum.UNSUPPORTED_FILTER));

        } catch (IOException e) {
            log.debug("Incorrect file error : {}", originalFilename);
            throw new ImageProcessingException(ProcessingErrorEnum.INCORRECT_PROCESS_FILE);
        }
    }

    private BufferedImage prepareImage(String originalName, BufferedImage image) {
        var customImage = CustomBufferedImage.builder()
                .setName(FilenameUtils.getBaseName(originalName))
                .setFormat(FilenameUtils.getExtension(originalName))
                .setSize(image.getWidth(), image.getHeight())
                .setType(image.getType())
                .build();
        var g = customImage.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return customImage;
    }
}
