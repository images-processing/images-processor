package com.image.processor.images.processing.strategies;

import com.image.processor.images.processing.services.ImageProcessorService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
class ImageProcessingStrategyConfig {

    @Bean
    public ImageProcessingStrategy imageProcessingStrategy(List<ImageProcessorService> imageProcessorServices) {
        return new ImageProcessingStrategyImpl(imageProcessorServices);
    }
}
