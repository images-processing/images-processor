package com.image.processor.images.provider;

import com.image.processor.enums.EffectEnum;
import com.image.processor.model.ImageInfoModel;
import com.image.processor.model.ImageInfoModelWithUsername;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FilenameUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static com.image.processor.utils.ImageFileUtils.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class ImageInfoModelMapper {

    private static final String EMPTY_STRING = "";

    static ImageInfoModel map(String[] fileNameCompositions, String username, String imageLocation) {
        var imageInfo = new ImageInfoModel();
        Optional.ofNullable(fileNameCompositions[0])
                .map(name -> getImageNameOnly(username, name, imageLocation))
                .ifPresent(imageInfo::setName);
        Optional.ofNullable(fileNameCompositions[1])
                .map(EffectEnum::map)
                .ifPresent(imageInfo::setEffect);
        Optional.ofNullable(fileNameCompositions[2])
                .map(dateTimeAndFileFormat -> setupExtension(imageInfo, dateTimeAndFileFormat))
                .ifPresent(dateTime -> setProcessTime(imageInfo, dateTime));
        return imageInfo;
    }

    static String map(ImageInfoModelWithUsername imageModel, String location) {
        return location + DIRECTORY_DELIMITER + imageModel.getUsername() + DIRECTORY_DELIMITER + imageModel.getName() +
                FILE_NAME_DELIMITER + imageModel.getEffect().effectName() + FILE_NAME_DELIMITER +
                imageModel.getProcessTime().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)) +
                EXTENSION_DELIMITER + imageModel.getFormat();
    }

    private static String getImageNameOnly(String username, String name, String imageLocation) {
        return name.replace(imageLocation, EMPTY_STRING).replace(username + DIRECTORY_DELIMITER, EMPTY_STRING);
    }

    private static String setupExtension(ImageInfoModel imageInfo, String dateTimeAndFileFormat) {
        var extension = FilenameUtils.getExtension(dateTimeAndFileFormat);
        imageInfo.setFormat(extension);
        return dateTimeAndFileFormat.replace(extension, EMPTY_STRING)
                .replace(EXTENSION_DELIMITER, EMPTY_STRING);
    }

    private static void setProcessTime(ImageInfoModel imageInfo, String dateTime) {
        imageInfo.setProcessTime(LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)));
    }
}
