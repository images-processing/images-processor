package com.image.processor.web;

import com.image.processor.images.ImageFacade;
import com.image.processor.model.ImageProcessModel;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
@RequiredArgsConstructor
class ImageProcessingController implements ImageController {

    private final ImageFacade imageFacade;

    @ApiOperation(value = "Processing image by suitable filter", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Process of image was started"),
            @ApiResponse(code = 400, message = "Invalid data")
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name="file", required = true, dataType = "__file", paramType = "form")
    })
    @PostMapping(value = UrlUtils.IMAGES_PROCESS)
    ResponseEntity process(@ModelAttribute @Valid final ImageProcessModel model) {
        log.debug("process() processingModel : {}", model);
        imageFacade.processImage(model);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
