package com.image.processor.web;

interface UrlUtils {

    String PREFIX = "/api";
    String IMAGES = PREFIX + "/images";
    String IMAGES_PROCESS = "/process";
    String IMAGES_GET_ALL = "/{username}";
}
