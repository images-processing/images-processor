package com.image.processor.web;

import com.image.processor.images.ImageFacade;
import com.image.processor.model.ImageInfoModel;
import com.image.processor.model.ImageInfoModelWithUsername;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;

@RestController
@Slf4j
@RequiredArgsConstructor
class ImageProvidingController implements ImageController {

    private final ImageFacade imageFacade;

    @ApiOperation(value = "providing processed images of user", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Images were successfully fetched")
    })
    @GetMapping(UrlUtils.IMAGES_GET_ALL)
    public ResponseEntity<Set<ImageInfoModel>> getAllImages(@PathVariable String username) {
        log.debug("Fetching images for user with name : {}", username);
        return ResponseEntity.status(HttpStatus.OK).body(imageFacade.fetchAllImages(username));
    }

    @ApiOperation(value = "Fetching image based on provided data")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Image was successfully fetched"),
            @ApiResponse(code = 400, message = "Image not found!")
    })
    @GetMapping
    public ResponseEntity<String> getImage(@ModelAttribute @Valid ImageInfoModelWithUsername imageInfoModel) {
        log.debug("Fetching image {} for user with name : {}", imageInfoModel, imageInfoModel.getUsername());
        return ResponseEntity.status(HttpStatus.OK).body(imageFacade.fetchImage(imageInfoModel));
    }
}
