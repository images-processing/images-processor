package com.image.processor.exceptions;

import com.image.processor.enums.ProcessingErrorEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ImageProcessingException extends RuntimeException {

    @Getter
    private final ProcessingErrorEnum error;
}
