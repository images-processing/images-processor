package com.image.processor.exceptions.handlers;

import com.image.processor.exceptions.ImageProcessingException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
@Setter
class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String INCORRECT_VALUE_MESSAGE = "Incorrect value!";
    private static final String TO_BIG_SIZE_OF_FILE_ERROR_MESSAGE = "Uploaded file exceeds admissible size, supported file size must be less then %s";

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxFileSize;

    @ExceptionHandler(ImageProcessingException.class)
    ResponseEntity<ProcessingErrorModel> handleProcessingErrors(ImageProcessingException exception) {
        log.error("Image processing error : {}", exception.getError().getMessage());
        var errorBody = ProcessingErrorModel.builder().addError(exception.getError()).build();
        return ResponseEntity.status(exception.getError().getStatus()).body(errorBody);
    }

    @ExceptionHandler(MultipartException.class)
    ResponseEntity<RequestError> handleToBigImageSize(MultipartException exception) {
        log.error("Image to big error : {}", exception.getMessage(), exception);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new RequestError("file", String.format(TO_BIG_SIZE_OF_FILE_ERROR_MESSAGE, maxFileSize)));
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Binding request parameter error : {}", request, ex);
        super.handleBindException(ex, headers, status, request);
        Set<RequestError> errors = ex.getBindingResult().getFieldErrors().stream().map(fieldError -> {
            if (fieldError.isBindingFailure()) {
                return new RequestError(fieldError.getField(), INCORRECT_VALUE_MESSAGE);
            } else {
                return new RequestError(fieldError.getField(), fieldError.getDefaultMessage());
            }
        }).collect(Collectors.toSet());
        return new ResponseEntity<>(errors, headers, status);
    }
}
