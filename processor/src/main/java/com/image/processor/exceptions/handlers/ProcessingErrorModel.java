package com.image.processor.exceptions.handlers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.image.processor.enums.ProcessingErrorEnum;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.ANY)
class ProcessingErrorModel {

    @Getter
    private final Set<Error> errors;

    static ProcessingErrorModelBuilder builder() {
        return new ProcessingErrorModelBuilder();
    }

    @EqualsAndHashCode
    @JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.ANY)
    @Getter
    private static class Error {
        private final ProcessingErrorEnum error;
        private final String message;

        Error(ProcessingErrorEnum error) {
            this.error = error;
            this.message = error.getMessage();
        }
    }

    static class ProcessingErrorModelBuilder {
        private final ProcessingErrorModel processingErrorModel;

        ProcessingErrorModelBuilder() {
            this.processingErrorModel = new ProcessingErrorModel(new HashSet<>());
        }

        ProcessingErrorModelBuilder addError(ProcessingErrorEnum error) {
            this.processingErrorModel.errors.add(new Error(error));
            return this;
        }

        ProcessingErrorModel build() {
            return processingErrorModel;
        }


    }
}
