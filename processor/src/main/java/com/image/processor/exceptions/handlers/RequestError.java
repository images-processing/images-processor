package com.image.processor.exceptions.handlers;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode(exclude = { "message" })
class RequestError {

    private final String property;
    private final String message;


}
