package com.image.processor.exceptions;

import java.util.function.Function;

@FunctionalInterface
public interface FunctionWithException<T, R, E extends Exception> {

    R apply(T t) throws E;

    static <T, R, E extends Exception> Function<T, R> wrapper(FunctionWithException<T, R, E> function, RuntimeException ex) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Exception e) {
                throw ex;
            }
        };
    }
}
