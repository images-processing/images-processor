package com.image.processor.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Data
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "spring.task.execution.pool")
class TaskExecutorConfig {

    private Integer maxSize;
    private Integer coreSize;

    @Bean
    public TaskExecutor processingExecutor() {
        log.info("Creating image processing task executor with max size : {} and core size : {}", maxSize, coreSize);
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(coreSize);
        executor.setMaxPoolSize(maxSize);
        executor.setThreadNamePrefix("image_processing_executor_thread");
        executor.initialize();
        return executor;
    }
}
