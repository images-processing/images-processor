package com.image.processor.config.processing;

import com.image.processor.enums.EffectEnum;
import com.jhlabs.image.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.awt.image.BufferedImageOp;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
class MapOfFiltersConfig {

    @Bean
    public Map<EffectEnum, BufferedImageOp> mapOfFilters() {
        log.info("Initializing base collection of filters");
        return new HashMap<>() {
            {
                put(EffectEnum.GRAYSCALE, new GrayscaleFilter());
                put(EffectEnum.INVERT, new InvertFilter());
                put(EffectEnum.LOOKUP, new LookupFilter());
                put(EffectEnum.MASK, new MaskFilter());
                put(EffectEnum.POSTERIZE, new PosterizeFilter());
                put(EffectEnum.SOLARIZE, new SolarizeFilter());
                put(EffectEnum.TRITONE, new TritoneFilter());
                put(EffectEnum.NOISE, new NoiseFilter());
                put(EffectEnum.EDGE, new EdgeFilter());
                put(EffectEnum.OPACITY, new OpacityFilter());
                put(EffectEnum.LAPLACE, new LaplaceFilter());
                put(EffectEnum.VARIABLE_BLUR, new VariableBlurFilter());
                put(EffectEnum.UNSHARP, new UnsharpFilter());
                put(EffectEnum.SMART_BLUR, new SmartBlurFilter());
                put(EffectEnum.SHARPEN, new SharpenFilter());
                put(EffectEnum.RAYS, new RaysFilter());
                put(EffectEnum.OIL, new OilFilter());
                put(EffectEnum.MOTION_BLUR, new MotionBlurFilter());
                put(EffectEnum.MINIMUM, new MinimumFilter());
                put(EffectEnum.MEDIAN, new MedianFilter());
                put(EffectEnum.MAXIMUM, new MaximumFilter());
                put(EffectEnum.LENS_BLUR, new LensBlurFilter());
                put(EffectEnum.GLOW, new GlowFilter());
                put(EffectEnum.GAUSSIAN, new GaussianFilter());
                put(EffectEnum.BUMP, new BumpFilter());
                put(EffectEnum.BLUR, new BlurFilter());
                put(EffectEnum.BOX_BLUR, new BoxBlurFilter());
                put(EffectEnum.BORDER_FILER, new BorderFilter());
                put(EffectEnum.BLOCK, new BlockFilter());
                put(EffectEnum.CHROME, new ChromeFilter());
                put(EffectEnum.CRYSTALLIZE, new CrystallizeFilter());
            }
        };
    }
}
