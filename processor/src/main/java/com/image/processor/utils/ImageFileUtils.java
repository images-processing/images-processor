package com.image.processor.utils;

public interface ImageFileUtils {

    String DIRECTORY_DELIMITER = "/";
    String EXTENSION_DELIMITER = ".";
    String FILE_NAME_DELIMITER = "_";
    String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String SINGLE_SPACE = " ";
    String EFFECT_ENUM_DELIMITER = "-";
}
