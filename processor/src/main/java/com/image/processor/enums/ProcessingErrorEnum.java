package com.image.processor.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
@Getter
public enum ProcessingErrorEnum {

    UNSUPPORTED_FILE_EXTENSION(HttpStatus.BAD_REQUEST, "File with this extension cannot be successfully processed!"),
    INCORRECT_PROCESS_FILE(HttpStatus.BAD_REQUEST, "Processing file is incorrect!"),
    UNSUPPORTED_FILTER(HttpStatus.BAD_REQUEST, "Unsupported filer!"),
    EMPTY_FILE(HttpStatus.BAD_REQUEST, "Empty file!");

    private final HttpStatus status;
    private final String message;

}
