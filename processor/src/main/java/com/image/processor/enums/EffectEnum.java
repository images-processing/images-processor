package com.image.processor.enums;

import com.image.processor.utils.ImageFileUtils;

public enum EffectEnum {

    GRAYSCALE,
    INVERT,
    LOOKUP,
    MASK,
    POSTERIZE,
    SOLARIZE,
    TRITONE,
    NOISE,
    EDGE,
    OPACITY,
    LAPLACE,
    VARIABLE_BLUR,
    UNSHARP,
    SMART_BLUR,
    SHARPEN,
    RAYS,
    OIL,
    MOTION_BLUR,
    MINIMUM,
    MEDIAN,
    MAXIMUM,
    LENS_BLUR,
    GLOW,
    GAUSSIAN,
    BUMP,
    BLUR,
    BOX_BLUR,
    BORDER_FILER,
    BLOCK,
    CHROME,
    CRYSTALLIZE;

    public String effectName() {
        return name().replace(ImageFileUtils.FILE_NAME_DELIMITER, ImageFileUtils.EFFECT_ENUM_DELIMITER);
    }

    public static EffectEnum map(String value) {
        return valueOf(value.replace(ImageFileUtils.EFFECT_ENUM_DELIMITER, ImageFileUtils.FILE_NAME_DELIMITER));
    }
}
