package com.image.processor.model.image;

import lombok.Getter;

import java.awt.image.BufferedImage;

@Getter
public final class CustomBufferedImage extends BufferedImage {

    private String imageName;
    private String imageFormat;

    private CustomBufferedImage(int width, int height, int imageType, String imageName, String imageFormat) {
        super(width, height, imageType);
        this.imageName = imageName;
        this.imageFormat = imageFormat;
    }

    public static ImageNameSetter builder() {
        return new CustomBufferedImageBuilder();
    }

    public static class CustomBufferedImageBuilder implements ImageNameSetter, ImageSizeSetter, ImageTypeSetter, ImageFormatSetter {
        private String name;
        private String format;
        private int width;
        private int height;
        private int imageType;

        @Override
        public ImageFormatSetter setName(String name) {
            this.name = name;
            return this;
        }

        @Override
        public ImageSizeSetter setFormat(String format) {
            this.format = format;
            return this;
        }

        @Override
        public ImageTypeSetter setSize(int width, int height) {
            this.width = width;
            this.height = height;
            return this;
        }

        @Override
        public CustomBufferedImageBuilder setType(int type) {
            this.imageType = type;
            return this;
        }

        public CustomBufferedImage build() {
            return new CustomBufferedImage(width, height, imageType, name, format);
        }
    }

    public interface ImageNameSetter {
        ImageFormatSetter setName(String name);
    }

    public interface ImageFormatSetter {
        ImageSizeSetter setFormat(String format);
    }
    public interface ImageSizeSetter {
        ImageTypeSetter setSize(int width, int height);

    }
    public interface ImageTypeSetter {
        CustomBufferedImageBuilder setType(int type);

    }
}
