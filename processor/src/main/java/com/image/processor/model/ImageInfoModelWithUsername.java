package com.image.processor.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ImageInfoModelWithUsername extends ImageInfoModel {

    @NotBlank
    private String username;
}
