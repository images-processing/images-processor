package com.image.processor.model;

import com.image.processor.enums.EffectEnum;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ImageProcessModel {

    @NotNull
    private volatile MultipartFile file;
    private EffectEnum effect;
    @NotNull
    @NotBlank
    @NotEmpty
    private String username;
}
