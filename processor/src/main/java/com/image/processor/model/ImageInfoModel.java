package com.image.processor.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.image.processor.enums.EffectEnum;
import com.image.processor.utils.ImageFileUtils;
import com.jhlabs.image.ImageUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Setter
@Getter
public class ImageInfoModel {

    @NotBlank
    private String name;
    @NotNull
    private EffectEnum effect;
    @NotNull
    @JsonFormat(pattern = ImageFileUtils.DATE_TIME_FORMAT)
    @DateTimeFormat(pattern = ImageFileUtils.DATE_TIME_FORMAT)
    private LocalDateTime processTime;
    @NotBlank
    private String format;
}
