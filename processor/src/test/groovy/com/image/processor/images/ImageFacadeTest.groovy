package com.image.processor.images

import com.image.processor.exceptions.ImageProcessingException
import com.image.processor.images.processing.services.ImageProcessorService
import com.image.processor.images.processing.strategies.ImageProcessingStrategy
import com.image.processor.images.provider.ImageProvider
import com.image.processor.images.store.ImageStore
import com.image.processor.model.ImageProcessModel
import org.springframework.core.task.TaskExecutor
import org.springframework.mock.web.MockMultipartFile
import spock.lang.Specification

class ImageFacadeTest extends Specification {

    private static final String TEST_FILE = "flower.png"

    private ClassLoader classLoader
    private InputStream imageFile

    def setup() {
        classLoader = new ImageFacadeTest().getClass().getClassLoader()
        imageFile = new FileInputStream(new File(classLoader.getResource(TEST_FILE).getFile()))
    }

    ImageProcessingStrategy processingStrategy = Mock(ImageProcessingStrategy)
    ImageProvider provider = Mock(ImageProvider)
    ImageStore store = Mock(ImageStore)
    TaskExecutor executor = Mock(TaskExecutor)
    def imageProcessorFacade = new ImageFacadeImpl(processingStrategy, store, provider, executor)

    def "should throw exception if getting processing service will throw exception"() {
        when:
            imageProcessorFacade.processImage(new ImageProcessModel())

        then:
            1 * processingStrategy.getProcessingService(_) >> { throw new ImageProcessingException() }
            thrown(ImageProcessingException)
    }

    def "should process images if strategy find processing service"() {
        given:
            def model = new ImageProcessModel()
            model.setFile(new MockMultipartFile("name", imageFile))

        when:
            imageProcessorFacade.processImage(model)

        then:
            1 * processingStrategy.getProcessingService(_) >> Mock(ImageProcessorService)
            noExceptionThrown()
    }
}
