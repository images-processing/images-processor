package com.image.processor.images.processing.strategies

import com.image.processor.enums.ProcessingErrorEnum
import com.image.processor.exceptions.ImageProcessingException
import com.image.processor.model.ImageProcessModel
import com.image.processor.images.processing.services.ImageProcessorService
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification
import spock.lang.Unroll

class ImageProcessingStrategyTest extends Specification {

    List<ImageProcessorService> imageProcessorServices = Arrays.asList(Mock(ImageProcessorService))
    def imageProcessingStrategy = new ImageProcessingStrategyImpl(imageProcessorServices)

    @Unroll
    def "should throw ImageProcessingException when file is #file"() {
        given:
            def processorModel = new ImageProcessModel();
            processorModel.setFile(file)

        when:
            imageProcessingStrategy.getProcessingService(processorModel)

        then:
            def exception = thrown(ImageProcessingException)
            exception != null
            ProcessingErrorEnum.UNSUPPORTED_FILE_EXTENSION == exception.getError()

        where:
            file                    || _
            null                    || _
            buildExampleFile(null)  || _
            buildExampleFile("")    || _
            buildExampleFile(" ")   || _
            buildExampleFile("xx")  || _
            buildExampleFile("xx.") || _
            buildExampleFile("x.x") || _
    }

    @Unroll
    def "should return ImageProcessorService when file is #file and extension is supported"() {
        given:
            def processorModel = new ImageProcessModel();
            processorModel.setFile(file)

        when:
            def imageProcessorService = imageProcessingStrategy.getProcessingService(processorModel)

        then:
            imageProcessorServices.stream().forEach({ service -> 1 * service.support(_) >> true })
            assert imageProcessorService != null

        where:
            file                                    || _
            buildExampleFile("name.xx")             || _
            buildExampleFile("name.with.dots.xx")   || _
    }



    MultipartFile buildExampleFile(String name) {
        return new MockMultipartFile("xx", name, "xx", new byte[0])
    }
}
