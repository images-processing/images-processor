package com.image.processor.images.processing.services

import com.image.processor.enums.EffectEnum
import com.image.processor.enums.ProcessingErrorEnum
import com.image.processor.exceptions.ImageProcessingException
import com.image.processor.model.ImageProcessModel
import org.springframework.mock.web.MockMultipartFile
import spock.lang.Specification
import spock.lang.Unroll

import java.awt.image.BufferedImage
import java.awt.image.BufferedImageOp

class ImageProcessorServiceTest extends Specification {

    private static final List<String> SINGLE_IMAGE_PROCESSOR_SUPPORTED_FILE_EXTENSIONS = ["jpeg", "jpg", "png"]
    private static final String TEST_FILE = "flower.png"

    def facade = new ImageProcessorServiceFacade()
    private ClassLoader classLoader
    private InputStream imageFile

    def setup() {
        classLoader = new ImageProcessorServiceTest().getClass().getClassLoader()
        imageFile = new FileInputStream(new File(classLoader.getResource(TEST_FILE).getFile()))
    }

    @Unroll
    def "should return image processor service by invoking method #serviceReturnMethod"() {
        when:
            def service = facade."$serviceReturnMethod"(_, _ as Map)

        then:
            service != null

        where:
            serviceReturnMethod             || _
            "singleImageProcessorService"   || _
    }

    @Unroll
    def "should return #correctResult from #serviceMethod for #fileExtension"() {
        given:
            ImageProcessorService service = facade."$serviceMethod"(supportedExtensions, _ as Map)

        when:
            def result = service.support(fileExtension)

        then:
            result == correctResult

        where:
            serviceMethod                   || fileExtension    || correctResult    || supportedExtensions
            "singleImageProcessorService"   || "jpg"            || true             || SINGLE_IMAGE_PROCESSOR_SUPPORTED_FILE_EXTENSIONS
            "singleImageProcessorService"   || "jpeg"           || true             || SINGLE_IMAGE_PROCESSOR_SUPPORTED_FILE_EXTENSIONS
            "singleImageProcessorService"   || "png"            || true             || SINGLE_IMAGE_PROCESSOR_SUPPORTED_FILE_EXTENSIONS
    }

    @Unroll
    def "should throw exception for #serviceMethod if file is empty"() {
        given:
            ImageProcessorService service = facade."$serviceMethod"(supportedExtensions, _ as Map)
            def model = new ImageProcessModel()
            model.setFile(new MockMultipartFile("name", new byte[0]))

        when:
            service.process(model)

        then:
            def exception = thrown(ImageProcessingException)
            ProcessingErrorEnum.INCORRECT_PROCESS_FILE == exception.getError()

        where:
            serviceMethod                 || supportedExtensions
            "singleImageProcessorService" || SINGLE_IMAGE_PROCESSOR_SUPPORTED_FILE_EXTENSIONS
    }

    @Unroll
    def "#serviceMethod should process image and return #expectedImageAmount images"() {
        given:
            Map<EffectEnum, BufferedImageOp> mapOfFilters = Mock(Map)
            BufferedImageOp filter = Mock(BufferedImageOp)
            ImageProcessorService service = facade."$serviceMethod"(supportedExtensions, mapOfFilters)
            def model = new ImageProcessModel()
            model.setEffect(effect)
            model.setFile(new MockMultipartFile("name", imageFile.bytes))

        when:
            def processedImages = service.process(model)

        then:
            1 * mapOfFilters.get(effect) >> filter
            1 * filter.filter(_, _) >> new BufferedImage(1, 1, 1)
            processedImages.size() == expectedImageAmount

        where:
            serviceMethod                 || supportedExtensions                              || expectedImageAmount || effect
            "singleImageProcessorService" || SINGLE_IMAGE_PROCESSOR_SUPPORTED_FILE_EXTENSIONS || 1                   || EffectEnum.GRAYSCALE
    }
}
