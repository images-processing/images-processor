FROM openjdk:11-jre-slim
COPY ./web/target/image-processor.jar /usr/src/image-processor/
WORKDIR /usr/src/image-processor
EXPOSE 8080
CMD ["java", "-jar", "image-processor.jar"]