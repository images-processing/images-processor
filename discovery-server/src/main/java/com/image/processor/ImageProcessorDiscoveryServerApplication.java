package com.image.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ImageProcessorDiscoveryServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImageProcessorDiscoveryServerApplication.class, args);
    }
}
