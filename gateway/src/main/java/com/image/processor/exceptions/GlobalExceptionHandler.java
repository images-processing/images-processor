package com.image.processor.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String TO_BIG_SIZE_OF_FILE_ERROR_MESSAGE = "Uploaded file exceeds admissible size, supported file size must be less then %s";
    private static final String MAX_FILE_SIZE = "10MB";

    @ExceptionHandler(MultipartException.class)
    ResponseEntity<Map> handleToBigImageSize(MultipartException exception) {
        Map<String, String> response = new HashMap<>();
        response.put("property", "file");
        response.put("message", String.format(TO_BIG_SIZE_OF_FILE_ERROR_MESSAGE, MAX_FILE_SIZE));
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST).body(response);
    }
}